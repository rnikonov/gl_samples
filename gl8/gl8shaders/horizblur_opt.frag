#version 330

uniform sampler2D tex;

in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec2 size = textureSize(tex, 0);
    vec2 dx = vec2(1.0 / size.x, 0.0); //шаг по горизонтали
    vec2 sdx = dx;
    vec4 sum = texture(tex, texCoord) * 0.134598;

	//Размытие по горизонтали по Гауссу, оптимизированное за счёт билинейной фильтрации

    const float weights[6] = float[6](0.127325f, 0.107778f, 0.081638f, 0.055335f, 0.033562f, 0.018216f/*, 0.008847f*/);
    const float weights_bilinear[3] = float[3](weights[0] + weights[1], weights[2] + weights[3], weights[4] + weights[5]);
    const float weights_bilinear_inv[3] = float[3](1 / weights_bilinear[0], 1 / weights_bilinear[1], 1 / weights_bilinear[2]);

    for (int i = 0; i < 3; i++) {
        int j = 2*i+1;
        vec2 sdx_sample = sdx + dx * (weights[j] * weights_bilinear_inv[i]);
        sum += (texture(tex, texCoord + sdx_sample) + texture(tex, texCoord - sdx_sample)) * weights_bilinear[i];
        sdx += dx + dx;
    }

    fragColor = sum;
}
