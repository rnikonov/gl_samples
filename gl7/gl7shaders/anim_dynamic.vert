#version 330

layout (location = 0) in vec3 attrVertexPos;
layout (location = 2) in vec2 attrTexCoords;
layout (location = 3) in vec4 attrBoneWeights;
layout (location = 4) in uvec4 attrBoneIds;

out vec2 texCoords;

out vec4 boneWeights;

const int MAX_BONES = 100;

uniform mat4 PVM;

// It is the worst idea ever to use fixed-size array but, well, it's just an example :)
uniform mat4 boneMatrices[MAX_BONES];

void main() {
	texCoords = attrTexCoords;

	mat4 boneTransform =
	    attrBoneWeights[0] * boneMatrices[attrBoneIds[0]] +
	    attrBoneWeights[1] * boneMatrices[attrBoneIds[1]] +
	    attrBoneWeights[2] * boneMatrices[attrBoneIds[2]] +
	    attrBoneWeights[3] * boneMatrices[attrBoneIds[3]];

	gl_Position = PVM * boneTransform * vec4(attrVertexPos, 1);
}
